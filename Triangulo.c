#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	int base;
	int altura;
	int resultado;
}triangulo;

int main()
{
	triangulo *area = (triangulo *) malloc(sizeof(triangulo));
	printf("Ingresa base: ");
	scanf("%d", &area->base);

	printf("Ingresa altura: ");
	scanf("%d", &area->altura);
	area->resultado = ((area->base) * (area->altura) )/ 2;
	
	printf("Resultado: %d\n", area->resultado);
}
//Created by Oliver Guadalupe Reyes Flores 
//803 Programación lógica y Funcional 
