public class LambdaPruebaMejorado{
    public static void main(String[] args){
        OperacionesAritmeticas op = (num1,num2) -> System.out.println("Resultado: " + (num1 + num2));
        op.miFuncion(30,50);
        LambdaPruebaMejorado objeto = new LambdaPruebaMejorado();
        objeto.miMetodo((num1, num2) -> System.out.println("Resultado: " + (num1 - num2)), 20, 30);
    }

    public void miMetodo(OperacionesAritmeticas op, int num1, int num2){
        op.miFuncion(num1,num2);
    }
}